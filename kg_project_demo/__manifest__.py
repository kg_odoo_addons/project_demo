# -*- coding: utf-8 -*-
{
    'name': 'Project Demo',
    'version': '12.0.1',
    'summary': 'Klystron Global Project ERP Demo Module',
    'description': 'Enterprise Odoo ERP Implementation for Demo Purpose',
    'category': 'Other',
    'author': 'KG',
    'license': 'AGPL-3',
    'website': 'www.klystronglobal.com',
    'depends': ['project', 'mrp_plm', 'mrp'],
    'data': [
        'security/ir.model.access.csv',
        'views/inherited_view.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,

}
