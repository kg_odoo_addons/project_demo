# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _
from odoo.addons import decimal_precision as dp


class KgProject(models.Model):
    _name = 'kg.project'
    _rec_name = 'name'

    name = fields.Char(string="Name")
    type = fields.Selection(string="Type", selection=[('amc', 'AMC'), ('gmc', 'GMC'), ('project', 'Project')])

    amc_count = fields.Integer(compute='_display_count_amc')
    gmc_count = fields.Integer(compute='_display_count_gmc')
    project_count = fields.Integer(compute='_display_count_project')

    @api.multi
    def _display_count_amc(self):
        for rel in self:
            count_id = self.env['project.project'].search([('service_id.type', '=', 'amc')])
            self.amc_count = len(count_id)

    @api.multi
    def _display_count_gmc(self):
        for rel in self:
            count_id = self.env['project.project'].search([('service_id.type', '=', 'gmc')])
            self.gmc_count = len(count_id)

    @api.multi
    def _display_count_project(self):
        for rel in self:
            count_id = self.env['project.project'].search([('service_id.type', '=', 'project')])
            self.project_count = len(count_id)

    @api.multi
    def button_amc(self):
        domain = [('service_id.type', '=', 'amc')]
        project_form_id = self.env.ref('project.edit_project').id
        project_kanban_id = self.env.ref('project.view_project_kanban').id
        return {
            'name': _('AMC'),
            'type': 'ir.actions.act_window',
            'domain': domain,
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'res_model': 'project.project',
            'view_id': project_kanban_id,
            'views': [(project_kanban_id, 'kanban'), (project_form_id, 'form')],
            'target': 'current',
        }

    @api.multi
    def button_gmc(self):
        domain = [('service_id.type', '=', 'gmc')]
        project_form_id = self.env.ref('project.edit_project').id
        project_kanban_id = self.env.ref('project.view_project_kanban').id
        return {
            'name': _('GMC'),
            'type': 'ir.actions.act_window',
            'domain': domain,
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'res_model': 'project.project',
            'view_id': project_kanban_id,
            'views': [(project_kanban_id, 'kanban'), (project_form_id, 'form')],
            'target': 'current',
        }

    @api.multi
    def button_project(self):
        domain = [('service_id.type', '=', 'project')]
        project_form_id = self.env.ref('project.edit_project').id
        project_kanban_id = self.env.ref('project.view_project_kanban').id
        return {
            'name': _('Project'),
            'type': 'ir.actions.act_window',
            'domain': domain,
            'view_type': 'form',
            'view_mode': 'kanban,tree,form',
            'res_model': 'project.project',
            'view_id': project_kanban_id,
            'views': [(project_kanban_id, 'kanban'), (project_form_id, 'form')],
            'target': 'project_kanban_id',
        }


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    product_qty = fields.Float(
        'Quantity To Service',
        default=1.0, digits=dp.get_precision('Product Unit of Measure'),
        readonly=True, required=True, track_visibility='onchange',
        states={'confirmed': [('readonly', False)]})


class ProjectProject(models.Model):
    _inherit = 'project.project'

    service_id = fields.Many2one("kg.project", string="Service")
    service_type = fields.Selection(string="Type", selection=[('amc', 'AMC'), ('gmc', 'GMC'), ('project', 'Project')],
                                    compute='_compute_service_type')

    @api.multi
    def _compute_service_type(self):
        for file in self:
            if file.service_id:
                file.service_type = file.service_id.type
